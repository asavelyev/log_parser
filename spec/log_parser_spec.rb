require 'spec_helper'

describe LogParser do
  subject(:log_parser) { described_class }

  let(:parser) { instance_double('Parser') }
  let(:printer) { instance_double('Printer') }
  let(:stats) { instance_double('Stats') }
  let(:filepath) { File.expand_path("../fixtures/test.log", __FILE__) }

  describe '.process' do
    before do
      allow(Parser).to receive(:new).and_return(parser)
      allow(Printer).to receive(:new).and_return(printer)
      allow(Stats).to receive(:new).and_return(stats)

      allow(parser).to receive(:collect).with(filepath, stats)
      allow(printer).to receive(:print_stats).with(stats)
    end

    context 'when file exists' do
      it 'processes succesfully and prints stats' do
        log_parser.process(filepath)

        expect(parser).to have_received(:collect).with(filepath, stats)
        expect(printer).to have_received(:print_stats).with(stats)
      end
    end

    context 'when file does not exist' do
      let(:filepath) { File.expand_path("../fixtures/test222.log", __FILE__) }

      it 'prints error' do
        expect { log_parser.process(filepath) }.to output("No such file, please make sure it exists\n").to_stdout

        expect(Parser).to_not receive(:new)
        expect(Printer).to_not receive(:new)
        expect(Stats).to_not receive(:new)
      end
    end
  end
end
