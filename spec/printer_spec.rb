require 'spec_helper'

describe Printer do
  subject(:printer) { described_class.new }

  let(:stats) { Stats.new }

  let(:printed) do
<<-EOL
Printing total stats: ===========
/about 3 visits
/index 1 visits

Printing uniq stats: ===========
/about 2 unique views
/index 1 unique views
EOL
  end

  before do
    stats.push '/about 1.1.1.1'
    stats.push '/about 1.1.1.2'
    stats.push '/about 1.1.1.2'
    stats.push '/index 1.1.1.3'
  end

  describe '#print_stats' do
    it 'prints data for each section' do
      expect { printer.print_stats(stats) }.to output(printed).to_stdout
    end
  end
end
