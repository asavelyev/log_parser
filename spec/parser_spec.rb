require 'spec_helper'

describe Parser do
  subject(:parser) { described_class.new }

  let(:stats) { [] }
  let(:filename) { File.expand_path("../fixtures/test.log", __FILE__) }

  before { parser.collect(filename, stats) }

  describe '#collect' do
    it 'processes all lines in log' do
      expect(stats.size).to eq(7)
    end
  end
end
