require 'spec_helper'

describe Stats do
  subject(:stats) { described_class.new }

  describe 'initialize' do
    it 'holds data' do
      expect(stats.data).to be_an_instance_of(Hash)
    end

    it 'has_default_values' do
      expect(stats.data['unknown']).to include({ count: 0, uniq: a_kind_of(Set) })
    end
  end

  describe '#push' do
    before do
      stats.push '/about 1.1.1.1'
      stats.push '/about 1.1.1.2'
      stats.push '/about 1.1.1.2'
      stats.push '/index 1.1.1.3'
    end

    it 'adds line to stats' do
      expect(stats.data['/about'][:count]).to eq(3)
      expect(stats.data['/about'][:uniq]).to include('1.1.1.1')
      expect(stats.data['/about'][:uniq]).to include('1.1.1.2')
      expect(stats.data['/about'][:uniq_count]).to eq(2)

      expect(stats.data['/index'][:count]).to eq(1)
      expect(stats.data['/index'][:uniq]).to include('1.1.1.3')
    end
  end
end
