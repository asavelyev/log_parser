require 'simplecov'
SimpleCov.start

require 'set'
require_relative './../lib/parser'
require_relative './../lib/printer'
require_relative './../lib/stats'
require_relative './../lib/log_parser'
