## How to use
```
run ./lib/log_parser.rb <path to file>
```
If file does not exists you will get an error message.

## Notes

- There are no validation on the file srtucture so I expect it to be like a test one.
- The data structure will not be optimal for a large files with lots of lines.
