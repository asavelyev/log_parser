require 'forwardable'

class Stats
  extend Forwardable

  attr_reader :data

  def_delegator :@data, :sort_by

  def initialize
    @data = Hash.new do |hash, key|
      hash[key] = { count: 0, uniq_count: 0, uniq: Set.new }
    end
  end

  def push(line)
    page, ip = line.split
    @data[page][:count] += 1
    @data[page][:uniq] << ip
    # maybe not the best option but saves sort by size of set in hash
    @data[page][:uniq_count] = @data[page][:uniq].size
  end
end
