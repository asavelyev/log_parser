#!/usr/bin/env ruby

require_relative 'parser'
require_relative 'stats'
require_relative 'printer'
require 'pry'
require 'set'

module LogParser
  def self.process(filepath)
    if File.exists?(filepath)
      parser = Parser.new
      printer = Printer.new
      stats = Stats.new

      parser.collect(filepath, stats)
      printer.print_stats(stats)
    else
      puts "No such file, please make sure it exists"
    end
  end
end

LogParser.process(ARGV.first.to_s)
