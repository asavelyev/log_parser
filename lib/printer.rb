class Printer
  def print_stats(stats)
    puts "Printing total stats: ==========="
    stats.sort_by { |page, data| -data[:count] }.each do |page, data|
      puts [page, data[:count], 'visits'].join(' ')
    end
    puts
    puts "Printing uniq stats: ==========="
    stats.sort_by { |page, data| -data[:uniq_count] }.each do |page, data|
      puts [page, data[:uniq_count], 'unique views'].join(' ')
    end
  end
end
